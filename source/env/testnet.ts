export default {
    production: false,
    network: 'mainnet',
    node: 'https://mainnet.tezrpc.me:443',
    helperbitApi: 'https://testnetbe.helperbit.com/api/v1/'
};
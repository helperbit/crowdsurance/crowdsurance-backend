import log = require('./log');
import { Async } from './async';

export default class Scheduler {
	tasks: { fn: () => void, timeout?: number, firstRun?: boolean; onStart?: boolean; interval?: NodeJS.Timeout }[];
	tasksonblock: { fn: () => void, firstRun: boolean, onStart: boolean }[];

	constructor() {
		this.tasks = [];
		this.tasksonblock = [];
	}

	addTask(fn, timeout, firstRun: boolean = true, onStart: boolean = false) {
		if (!timeout)
			timeout = false;

		this.tasks.push({ fn: fn, timeout: timeout, firstRun: firstRun, onStart: onStart });
	}

	addOnStartTask(fn) {
		this.tasks.push({ fn: fn, firstRun: true });
	}

	addOnBlockTask(fn, firstRun: boolean = true) {
		this.tasksonblock.push({ fn: fn, firstRun: firstRun, onStart: false });
	}

	async start(): Promise<void> {
		log.debug('scheduler', `Starting ${this.tasks.length} jobs...`);

		await Async.forEach(this.tasks, async task => {
			if (task.onStart)
				await task.fn();
		});

		await Async.forEach(this.tasks, async task => {
			if (!task.onStart && task.firstRun)
				try {
					await task.fn();
				} catch(err) {
					console.log(task, err);
				}

			if (task.timeout)
				task.interval = setInterval(async () => { await task.fn(); }, task.timeout);
		});

		await Async.forEach(this.tasksonblock, async task => {
			if (task.firstRun)
				await task.fn();
		});
	}

	stop() { }
}


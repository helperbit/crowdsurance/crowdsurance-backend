import colors = require('colors/safe');
import cluster = require('cluster');

(colors as any).enabled = true;

const clfmonth = [
	'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
	'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
];

const pad2 = (str: number): string => {
	let str2: string = String(str);

	while (str2.length < 2)
		str2 = '0' + str2;

	return str2;
};

const clfDate = (): string => {
	const dateTime: Date = new Date();
	const date: number = dateTime.getUTCDate();
	const hour: number = dateTime.getUTCHours();
	const mins: number = dateTime.getUTCMinutes();
	const secs: number = dateTime.getUTCSeconds();
	const year: number = dateTime.getUTCFullYear();
	const month: string = clfmonth[dateTime.getUTCMonth()];

	//if (config.logs.date)
	return `[${pad2(date)}/${month}/${year}:${pad2(hour)}:${pad2(mins)}:${pad2(secs)} +0000] `;
	// else
	// 	return '';
};


const workerInfo = () => {
	if (cluster.worker)
		return colors.red('worker' + cluster.worker.id) + ' - ';
	else
		return ''
};

export function info(mod: string, message: string, req?: Request){
	console.log(colors.magenta(clfDate()) + workerInfo() + colors.yellow(mod) + ' - ' + colors.cyan(message));
}

export function debug(mod: string, message: string, req?: Request){
	console.log(colors.magenta(clfDate()) + workerInfo() + colors.cyan(mod) + ' - ' + colors.green(message));
}

export function debug2(mod: string, message: string, req?: Request){
	console.log(colors.magenta(clfDate()) + workerInfo() + colors.cyan(mod) + ' - ' + colors.grey(message));
}

export function critical(mod: string, message: string, req?: Request){
	console.log(colors.magenta(clfDate()) + workerInfo() + colors.yellow(mod) + ' - ' + colors.red(message));
}

export function error(mod: string, message: string, req?: Request){
	console.log(colors.magenta(clfDate()) + workerInfo() + colors.yellow(mod) + ' - ' + colors.red(message));
}

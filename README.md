# Crowdsurance backend

Handle contracts deployment, user verification, event triggers and statistical data.
This software acts as a bridge between our GIS system infrastructure, the helperbit account system
and the Crowdsurance contracts in the Tezos blockchain.


## Build & run

```bash
npm run build
npm start
```
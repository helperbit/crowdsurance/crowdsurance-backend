export namespace Async {
	type ForEachCallback<T> = (value: T, index?: number, array?: T[]) => Promise<any>;
	type MapCallback<T> = (value: T, index?: number, array?: T[]) => Promise<any>;

	export async function forEach<T>(array: T[], callback: ForEachCallback<T>) {
		for (let index = 0; index < array.length; index++) {
			await callback(array[index], index, array);
		}
		return true;
	}

	export async function map<T>(array: T[], callback: MapCallback<T>) {
		const na = [];
		for (let index = 0; index < array.length; index++) {
			na.push (await callback(array[index], index, array));
		}
		return na;
	}
}